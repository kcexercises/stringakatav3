﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Calculator
{
    public class StringCalculator
    {
        public int Add(string numbers)
        {
            if (String.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            var numbersList = GetSplittedDelimiteredStringNumbers(numbers);
            var numbersArray = Array.ConvertAll(numbersList, int.Parse);
            
            return Sum(numbersArray);
        }

        private int Sum(int[] numbersArray)
        {
            int sum = 0; 
            var negativeNumbers = new List<int>();

            foreach(var number in numbersArray.Where(value => value <= 1000))
            {
                if (number < 0)
                {
                    negativeNumbers.Add(number);
                }
                sum += number;
            }

            CheckForNegativeNumbers(negativeNumbers);

            return sum;
        }

        private void CheckForNegativeNumbers(List<int> negativeNumbers)
        {
            if (negativeNumbers.Any())
            {
               throw new Exception("Negatives not allowed: " + string.Join(",", negativeNumbers));
            }
        }

        private string[] GetSplittedDelimiteredStringNumbers(string numbers)
        {
            if (numbers.StartsWith("//")) 
            {
                return SplitDelimiteredStringNumbers(numbers);
            }
            
            return numbers.Split(new char[] {',','\n'});
        }

        private string[] SplitDelimiteredStringNumbers(string numbers)
        {
            var numbersAndDelimiters = numbers.Split("\n");

            if (numbers[2] == '[')
            {
                numbersAndDelimiters[0] = numbersAndDelimiters[0].Replace("//","");
                var delimiters = numbersAndDelimiters[0].Split(new char[] { '[', ']'}, StringSplitOptions.RemoveEmptyEntries);
                return numbersAndDelimiters[1].Split(delimiters, StringSplitOptions.None);
            }
                numbersAndDelimiters = numbers.Split("\n");
                numbersAndDelimiters[0] = numbersAndDelimiters[0].Replace("//", "");
                return numbersAndDelimiters[1].Split(numbersAndDelimiters[0], StringSplitOptions.None);
        }
    }
}
