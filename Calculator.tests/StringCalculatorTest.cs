using System;
using NUnit.Framework;

namespace Calculator.tests
{
    public class StringCalculatorTest
    {
        private StringCalculator _stringCalculator;

        [SetUp]
        public void Init()
        {  
            _stringCalculator = new StringCalculator();
        }
        
        [Test]
        public void GIVEN_NullOrEmptyString_WHEN_Adding_RETURNS_Zero()
        {
            //arrange
            const int expectedResult = 0;
            const string numbers = "";

            //act
            var actualResult = _stringCalculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_TwoNumbersSeparatedByComma_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            const int expectedResult = 3;
            const string numbers = "1,2";

            //act
            var actualResult = _stringCalculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_UnknownAmountOfNumbersSeparatedByComma_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            const int expectedResult = 15;
            const string numbers = "1,2,3,7,2";

            //act
            var actualResult = _stringCalculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_NumbersSeparatedByNewlinesAndCommas_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            const int expectedResult = 21;
            const string numbers = "1\n2,3\n7\n8";

            //act
            var actualResult = _stringCalculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_NumbersWithCustomDelimiter_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            const int expectedResult = 3;
            const string numbers = "//;\n1;2";

            //act
            var actualResult = _stringCalculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_NumbersWithDelimitersOfAnyLength_WHEN_Adding_RETURNS_TheSum()
        {
            //arrange
            const int expectedResult = 6;
            const string numbers = "//***\n1***2***3";

            //act
            var actualResult = _stringCalculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);
        }

        [Test]
        public void GIVEN_NumbersWithMultipleDelimiters_WHEN_Adding_RETURNS_TheSum()
        {
           //arrange
            const int expectedResult = 6;
            const string numbers = "//[*][%]\n1*2%3";

            //act
            var actualResult = _stringCalculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);  
        }

        [Test]
        public void GIVEN_NumbersWithMultipleDelimitersOfAnyLength_WHEN_Adding_RETURNS_TheSum()
        {
           //arrange
            const int expectedResult = 7;
            const string numbers = "//[****][%%%]\n2****2%%%3";

            //act
            var actualResult = _stringCalculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult);  
        }

        [Test]
        public void GIVEN_StringWithNegativeNumbers_WHEN_Adding_SHOULD_ThrowExceptionMessageWithNegativeNumbers()
        {
            //arrange
            const string expectedResult = "Negatives not allowed: " + "-3,-8";
            const string numbers = "1\n2,-3\n7\n-8";

            //act
            var actualResult = Assert.Throws<Exception>(() => _stringCalculator.Add(numbers));

            //assert
            Assert.That(actualResult.Message, Is.EqualTo(expectedResult));
        }

        [Test]
        public void GIVEN_NumbersOverThousand_WHEN_Adding_RETURNS_TheSumExcludingNumbersOverThousand()
        {
            //arrange
            const int expectedResult = 4;
            const string numbers = "//***\n1***1001***3";

            //act
            var actualResult = _stringCalculator.Add(numbers);

            //assert
            Assert.AreEqual(expectedResult, actualResult); 
        }
    }
}